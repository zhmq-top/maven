<%--
  Created by IntelliJ IDEA.
  User: Glory
  Date: 2020-10-21 0021
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <link rel="stylesheet" href="css/style.css" />
  <link rel="icon" href="img/login.png" type="image/x-icon"> <!-- 添加登录标题图标 -->
  <title>登录界面</title>
  <meta charset="GBK">
</head>
<body>

<div id="bigBox">
  <h1>模拟商城</h1>
  <form action="LoginServlet" method="post">
    <div class="inputBox">
      <div class="inputText">
        <img src="img/user.png" class="user"  alt="">
        <label>
          <input type="text" name="userid" placeholder="Userid" />
        </label>
      </div>
      <div class="inputText">
        <img src="img/pwd.png" class="user"  alt="">
        <label>
          <input type="password" name="password" placeholder="Password" />
        </label>
      </div>
    </div>
    <input class= "loginButton" type="submit" value="模拟商城登录" />
    <br><br>
    <a href="register.jsp" class="user" type="submit"><font color="#00ffff">注册</font></a>
  </form>
</div>

</body>
</html>
