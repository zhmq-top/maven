package servlet;

import zut.DAO.impl.OrderDAOImpl;
import zut.DO.OrderDO;
import zut.DO.UserDO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "OrderServlet" ,urlPatterns = "/OrderServlet")
public class OrderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*
        *生成订单，用户支付界面
        * @Author:Glory
        * @Description:
        * @Date:10:26 2020-11-6 0006
        * @return:
        *
        */


        String username = (String) request.getSession().getAttribute("username");
        String pr = request.getParameter("price");
        String userid  = (String) request.getSession().getAttribute("userid");
        OrderDO order = new OrderDO(userid,"未支付",Double.parseDouble(pr));
        OrderDAOImpl odi = new OrderDAOImpl();
        request.getSession().setAttribute("order",order);
        odi.addOrder(order);
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML>");
        out.println("<HTML>");
        out.println("  <HEAD><TITLE>支付界面</TITLE></HEAD>");
        out.println("  <BODY>");
        out.println("<td>请核对信息</td>");
        out.println("<td>订单编号（自动生成）："+"</td>");
        out.println("<br><td>用户名："+username+"</td>");
        out.println("<br><td>总金额："+pr+"</td>");
        out.println("<br><td><a href='Ordersuccess'>"+pr+"确认支付</a></td>");
        out.println("  </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }
}
