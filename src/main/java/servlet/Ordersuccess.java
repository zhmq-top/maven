package servlet;

import zut.DO.OrderDO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Ordersuccess",urlPatterns = "/Ordersuccess")
public class Ordersuccess extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderDO order = (OrderDO) request.getSession().getAttribute("order");
//        String orderid = order.getOrderid();

        Double pr = order.getTotalPrice();
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML>");
        out.println("<HTML>");
        out.println("<HEAD><TITLE>支付</TITLE></HEAD>");
        out.println("<BODY>");
        out.println("<img src= 'img/1.jpg'><br>");
        out.println("<td>请支付"+pr+"<td>");
        out.println("<br><td>如您已经完成支付，您可</td>");
        out.println("<td><a href='GetOrdersServlet'>查询订单</a></td>");
        out.println("  </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }
}
