package servlet;

import zut.DAO.impl.AuctionDAOImpl;
import zut.DAO.impl.UserDAOImpl;
import zut.DO.AuctionDO;
import zut.DO.ResultDO;
import zut.DO.UserDO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "AuctionView" ,urlPatterns = "/AuctionView")
public class AuctionView extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*
        *根据是否登录成功返回对应的商品界面，不登录无操作权限
        * @Author:Glory
        * @Description:
        * @Date:10:22 2020-11-6 0006
        * @return:
        *
        */
        UserDAOImpl udl = new UserDAOImpl();
        ResultDO<UserDO> r = (ResultDO<UserDO>) request.getSession().getAttribute("user");
        String userid = (String) request.getSession().getAttribute("userid");
        UserDO user = udl.getUserDO(userid);
        if(user==null){
            List<AuctionDO> list = new AuctionDAOImpl().getall();
            response.setContentType("text/html;charset=gbk");
            PrintWriter out = response.getWriter();
            out.println("<!DOCTYPE HTML>");
            out.println("<HTML>");
            out.println("  <HEAD><TITLE>商品列表</TITLE></HEAD>");
            out.println("  <BODY>");
            out.println("<font color='red'>"+r.getInfo()+" </font>，登录失败，您现在只能查看商品");
            out.println("<a href='login.html'>重新登录</a>");
            out.println("<table border='1'>");
            out.println("<tr>");
            out.println("<th>商品序号</th>");
            out.println("<th>商品名称</th>");
            out.println("<th>商品价格</th>");
            out.println("<th>商品详情</th>");
            out.println("</tr>");
            for (int i = 0; i < list.size(); i++) {
                out.println("<tr align=\"center\"><form action='ShopCardController?type=addCard&id="+i+"' method='post'>");
                out.println("<td>" + list.get(i).getId() + "</td>");
                out.println("<td>" + list.get(i).getName() + "</td>");
                out.println("<td>" + list.get(i).getPrice() + "</td>");
                out.println("<td>" + list.get(i).getDetail() + "</td>");
                out.println("</form></tr>");
            }
            out.println("</table>");
            out.println("  </BODY>");
            out.println("</HTML>");
            out.flush();
            out.close();
        }
        else {
            String username = user.getUsername();
            request.getSession().setAttribute("username",username);
            List<AuctionDO> list = new AuctionDAOImpl().getall();
            response.setContentType("text/html;charset=utf-8");
            PrintWriter out = response.getWriter();
            out.println("<!DOCTYPE HTML>");
            out.println("<HTML>");
            out.println("  <HEAD><TITLE>商品列表</TITLE></HEAD>");
            out.println("  <BODY>");
            if(r.getStatus()){
                out.println("登陆成功，欢迎您，"+ username+" 。<br>");
                out.println("<a href='ShopCardController?type=cardList'>查看购物车</a>");
                out.println("<br><a href='GetOrdersServlet'>查询所有订单</a>");
            }
            else if(r.getInfo()!=null){
                out.println("<font color='red'>"+r.getInfo()+" </font>，登录失败，您现在只能查看商品");
                out.println("<a href='login.html'>重新登录</a>");
            }
            out.println("<table border='1'>");
            out.println("<tr>");
            out.println("<th>商品序号</th>");
            out.println("<th>商品名称</th>");
            out.println("<th>商品价格</th>");
            out.println("<th>商品详情</th>");
            if(r.getStatus()) {
                out.println("<th>购买数量</th>");
                out.println("<th>操作</th>");
            }
            out.println("</tr>");
            for (int i = 0; i < list.size(); i++) {
                out.println("<tr align=\"center\"><form action='ShopCardController?type=addCard&id="+i+"' method='post'>");
                out.println("<td>" + list.get(i).getId() + "</td>");
                out.println("<td>" + list.get(i).getName() + "</td>");
                out.println("<td>" + list.get(i).getPrice() + "</td>");
                out.println("<td>" + list.get(i).getDetail() + "</td>");
                if(r.getStatus()) {
                    out.println("<td><input type='text' name='number' value='1'></td>");
                    out.println("<td><input type='submit' value='加购'></td>");
                }
                out.println("</form></tr>");
            }
            out.println("</table>");
            out.println("  </BODY>");
            out.println("</HTML>");
            out.flush();
            out.close();
        }
    }
}
