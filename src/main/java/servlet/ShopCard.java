package servlet;

import zut.DO.AuctionDO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShopCard",urlPatterns = "/ShopCard")
public class ShopCard extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShopCard() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*
        * 查看购物车，含有相应操作及其跳转
        * @Author:Glory
        * @Description:
        * @Date:10:28 2020-11-6 0006
        * @return:
        *
        */
        request.setCharacterEncoding("utf-8");
        Object goodsObject = request.getSession().getAttribute("myGoodsList");
        List<AuctionDO> myGoodsList = null;
        if (goodsObject == null) {
            myGoodsList = new ArrayList<>();
            request.getSession().setAttribute("myGoodsList", myGoodsList);
        } else {
            myGoodsList = (ArrayList<AuctionDO>) goodsObject;
        }
        double totalPrice = 0.0;
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE HTML>");
        out.println("<HTML>");
        out.println("  <HEAD><TITLE>临时购物车</TITLE></HEAD>");
        out.println("  <BODY>");
        out.println("<a href='AuctionView'>继续购物</a>");
        out.println("<table border='1'>");
        out.println("<tr>");
        out.println("<td>商品序号</td>");
        out.println("<td>商品名称</td>");
        out.println("<td>商品单价</td>");
        out.println("<td>购买数量</td>");
        out.println("<td>总价</td>");
        out.println("<td>操作</td>");
        out.println("</tr>");
        for (int i = 0; i < myGoodsList.size(); i++) {
            AuctionDO goods = myGoodsList.get(i);
            out.println("<tr>");
            out.println("<td>" + (i + 1) + "</td>");
            out.println("<td>" + goods.getName() + "</td>");
            out.println("<td>" + goods.getPrice() + "</td>");
            out.println("<td>" + goods.getNumber() + "</td>");
            out.println("<td>" + goods.getPrice() * goods.getNumber() + "</td>");
            totalPrice += goods.getPrice()*goods.getNumber();
            out.println("<td><a href='ShopCardController?type=removeCard&id=" + i + "'>删除</a></td>");
            out.println("</tr>");
        }
        out.println("</table>");
        out.println("<td><a href='OrderServlet?price="+ totalPrice +"'>结算</a></td>");
        out.println("  </BODY>");
        out.println("</HTML>");
        out.flush();
        out.close();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
