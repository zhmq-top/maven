package servlet;

import zut.DO.OrderDO;
import zut.service.impl.OrderServiceImpl;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "GetOrdersServlet",urlPatterns = "/GetOrdersServlet")
public class GetOrdersServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderServiceImpl osi =new OrderServiceImpl();
        try {
            String userid = (String) request.getSession().getAttribute("userid");
            List<OrderDO> ls= osi.findordersbyid(userid);
            response.setContentType("text/html;charset=utf-8");
            PrintWriter out = response.getWriter();
            out.println("<!DOCTYPE HTML>");
            out.println("<HTML>");
            out.println("  <HEAD><TITLE>订单查询结果</TITLE></HEAD>");
            out.println("  <BODY>");
            if(ls.size()==0){
                out.println("暂无订单");
            }
            else {
                for (OrderDO l : ls) {
                    out.println("<br>"+l.toString()+"</br>");
                }
                out.println("<a href='AuctionView'>继续购物</a>");
            }
            out.println("  </BODY>");
            out.println("</HTML>");
            out.flush();
            out.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
