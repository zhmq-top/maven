package zut.DO;

import java.util.Date;

public class OrderDO {
    /*
    * 订单实体层
    * @Author:Glory
    * @Description:
    * @Date:10:31 2020-11-6 0006
    * @return:
    *
    */
    String orderid;
    String userid;
    String status;
    Double totalPrice;
    Date createtime;

    public OrderDO(String userid, String status, Double totalPrice) {
        this.userid = userid;
        this.status = status;
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "订单编号："+orderid +",用户id："+userid +",订单状态："+status + ",总金额："+totalPrice +",订单时间："+createtime;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
