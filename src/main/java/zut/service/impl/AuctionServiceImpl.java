package zut.service.impl;

import zut.DAO.impl.AuctionDAOImpl;
import zut.DO.AuctionDO;
import zut.service.AuctionService;

import java.util.List;

public class AuctionServiceImpl implements AuctionService {
    @Override
    public List<AuctionDO> getallaction() {
        return new AuctionDAOImpl().getall();
    }
}
