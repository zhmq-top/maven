//获取数据库用户信息的接口

package zut.DAO;

import zut.DO.UserDO;

public interface UserDAO {
    /**
     * 根据用户名获得一个用户的基本信息
     * @param userid
     * @return UserDO对象，包含当前用户的登录状态
     */
    UserDO getUserDO(String userid);


    /**
     * 将一个用户信息存储到数据库中
     * @param user
     * @return
     */
    boolean insertUserDO(UserDO user);

    boolean deleteUserDO(UserDO user);

}
